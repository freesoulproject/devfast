package com.project.free_soul_food.devfast.Model;

import com.project.free_soul_food.devfast.Enum.StatusMotoboy;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by Free_Soul_Food on 14/09/2017.
 *
 * **/
public class Motoboy implements Serializable {

    private Long idMotoboy;
    private String nome;
    private String cnh;
    private String cpf;
    private String telefone;
    private String foto;
    private StatusMotoboy statusMotoboy;
    private Moto moto;

    private Login login;

    private Endereco endereco;

    public Long getIdMotoboy() {
        return idMotoboy;
    }

    public void setIdMotoboy(Long idMotoboy) {
        this.idMotoboy = idMotoboy;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnh() {
        return cnh;
    }

    public void setCnh(String cnh) {
        this.cnh = cnh;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getTelefoneMotoboy() {
        return telefone;
    }

    public void setTelefoneMotoboy(String telefoneMotoboy) {
        this.telefone= telefoneMotoboy;
    }

    public StatusMotoboy getStatusMotoboy() {
        return statusMotoboy;
    }

    public void setStatusMotoboy(StatusMotoboy statusMotoboy) {
        this.statusMotoboy = statusMotoboy;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Moto getMoto() {
        return moto;
    }

    public void setMoto(Moto moto) {
        this.moto = moto;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
}
