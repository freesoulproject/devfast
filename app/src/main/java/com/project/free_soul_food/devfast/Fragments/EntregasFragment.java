package com.project.free_soul_food.devfast.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.free_soul_food.devfast.Activity.DetalhesActivity;
import com.project.free_soul_food.devfast.Adapter.RVEntregasAdapter;
import com.project.free_soul_food.devfast.Model.Entrega;
import com.project.free_soul_food.devfast.R;
import com.project.free_soul_food.devfast.Rest.JsonParser;
import com.project.free_soul_food.devfast.Rest.RestAddress;
import com.project.free_soul_food.devfast.task.HandlerTask;
import com.project.free_soul_food.devfast.task.HandlerTaskAdapter;
import com.project.free_soul_food.devfast.task.TaskRest;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class EntregasFragment extends Fragment {
    View view;

    RecyclerView recyclerEntregas;
    List<Entrega> entregas = new ArrayList<>();
    Entrega entrega;
    //JsonParser<Entrega> parserEntrega = new JsonParser<>(Entrega.class);
    SwipeRefreshLayout swipeRefreshEntrega;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_entregas, container, false);

        createSwipeRefresh();
        getEntrega();

        return view;
    }


    // cria o recyclerview
    private void createRecyclerView(List<Entrega> entregas) {
        recyclerEntregas = view.findViewById(R.id.recycler_entregas);
        recyclerEntregas.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerEntregas.setHasFixedSize(true);
        // instancia o adaptador
        RVEntregasAdapter adapter = new RVEntregasAdapter(entregas, getContext(), onClickEntrega());
        recyclerEntregas.setAdapter(adapter);
    }

    private RVEntregasAdapter.EntregasOnClickListener onClickEntrega() {
        return new RVEntregasAdapter.EntregasOnClickListener() {
            @Override
            public void onClickEntregas(View view, int position) {
                // Recupera uma entrega da lista
                entrega = entregas.get(position);
                // registrar o menu de context no recyclerview
                registerForContextMenu(recyclerEntregas);
                // abre o menu de contexto
                getActivity().openContextMenu(view);
            }
        };
    }

    // executa a asynctask
    private void getEntrega() {
        // executa a asynctask
        new TaskRest(TaskRest.RequestMethod.GET, handlerListar, true, createSwipeRefresh()).execute(RestAddress.listaEntrega);
    }

    private SwipeRefreshLayout createSwipeRefresh() {
        swipeRefreshEntrega = view.findViewById(R.id.swipe_entregas);
        // associa um listener ao swipeRefresh
        swipeRefreshEntrega.setOnRefreshListener(onRefreshEntrega());
        return this.swipeRefreshEntrega;
    }

    // listener para o swipeRefresh
    private SwipeRefreshLayout.OnRefreshListener onRefreshEntrega() {
        return new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getEntrega();
            }
        };
    }

    // handler
    private HandlerTask handlerListar = new HandlerTaskAdapter() {
        @Override
        public void onSuccess(String readValue) {
            super.onSuccess(readValue);
            // cria uma instancia de jsonparser
            JsonParser<Entrega> parserEntrega = new JsonParser<>(Entrega.class);
            // inicializa a lista de entregas convertendo json[] para list<Cliente>
            entregas = parserEntrega.toList(readValue, Entrega[].class);
            // passa a lista de entregas para criar o recyclerview
            createRecyclerView(entregas);
        }

        @Override
        public void onError(Exception erro) {
            super.onError(erro);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        // Executa a task de consulta GET
        getEntrega();
    }


    public void recusarOnClick() {

    }


}
