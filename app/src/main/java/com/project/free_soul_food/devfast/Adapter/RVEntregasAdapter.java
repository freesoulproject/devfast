package com.project.free_soul_food.devfast.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.project.free_soul_food.devfast.Model.Entrega;
import com.project.free_soul_food.devfast.R;

import java.util.List;

/**
 * Created by Free_Soul_Food on 18/09/2017.
 */

public class RVEntregasAdapter extends RecyclerView.Adapter<RVEntregasAdapter.EntregasViewHolder> {
    private final List<Entrega> entregas;
    private final Context context;
    final EntregasOnClickListener onClickListener;

    public RVEntregasAdapter(List<Entrega> entregas, Context context, EntregasOnClickListener onClickListener) {
        this.entregas = entregas;
        this.onClickListener = onClickListener;
        this.context = context;
    }

    // interface de listener
    public interface EntregasOnClickListener {
        void onClickEntregas(View view, int position);
    }


    @Override
    public EntregasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_entregas, parent, false);

        return new EntregasViewHolder(view);
    }


    @Override
    public void onBindViewHolder(EntregasViewHolder holder, final int position) {

        String informaçoes = entregas.get(position).getTamanho() + "," + entregas.get(position)
                .getFragilidade();
        holder.nomeCliente.setText(entregas.get(position).getNomeCliente());
        holder.bairroEntrega.setText(entregas.get(position).getEndereco().getBairro());
        //holder.nomeRestaurante.setText(entregas.get(position).getRestaurante().getNome());
        holder.informacoesEntrega.setText(informaçoes);
        holder.descricaoEntrega.setText(entregas.get(position).getDescricao());
        if (onClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListener.onClickEntregas(view, position);
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return entregas != null ? entregas.size() : 0;
    }

    public class EntregasViewHolder extends RecyclerView.ViewHolder {
        //viewsa

        CardView cardEntregas;
        TextView nomeCliente;
        TextView nomeRestaurante;
        TextView bairroEntrega;
        TextView descricaoEntrega;
        TextView informacoesEntrega;
        View itemView;



        public EntregasViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            //faz o findviewbyid

            cardEntregas = (CardView) itemView.findViewById(R.id.card_entregas);

            nomeCliente = (TextView) itemView.findViewById(R.id.nome_cliente);
            nomeRestaurante = (TextView) itemView.findViewById(R.id.nome_restaurante);
            bairroEntrega = (TextView) itemView.findViewById(R.id.bairro_entrega);
            descricaoEntrega = (TextView) itemView.findViewById(R.id.descricao_entrega);
            informacoesEntrega = (TextView) itemView.findViewById(R.id.informacoes_entrega);


        }

    }
}
