package com.project.free_soul_food.devfast.Model;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by Free_Soul_Food on 14/09/2017.
 */

public class Login implements Serializable{
    private Long idLogin;
    private String email;
    private String senha;
    private String token;

    public Long getIdLogin() {
        return idLogin;
    }

    public void setIdLogin(Long idLogin) {
        this.idLogin = idLogin;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
