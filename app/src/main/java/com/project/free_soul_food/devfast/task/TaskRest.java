package com.project.free_soul_food.devfast.task;

import android.app.Activity;
import android.content.Intent;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.JsonReader;
import android.util.Log;
import android.widget.Toast;

import com.project.free_soul_food.devfast.Activity.AppActivity;
import com.project.free_soul_food.devfast.Activity.MainActivity;
import com.project.free_soul_food.devfast.Model.Entrega;
import com.project.free_soul_food.devfast.Model.Motoboy;
import com.project.free_soul_food.devfast.Utils.ReadStreamUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;

public class TaskRest extends AsyncTask<String, SwipeRefreshLayout, String>  {

    // Métodos http
    public enum RequestMethod {
        GET, POST, PUT, DELETE
    }

    // Controle de exceções
    private Exception exception;

    // método http
    private RequestMethod requestMethod;

    // redrecionador
    private HandlerTask handlerTask;

    // Controle do swipe refresh
    private boolean refresh;

    private SwipeRefreshLayout swipeRefreshLayout;

    // Contrutor
    public TaskRest(RequestMethod requestMethod, HandlerTask handlerTask) {
        this.requestMethod = requestMethod;
        this.handlerTask = handlerTask;
    }

    // Contrutor e sobrescrita
    public TaskRest(RequestMethod requestMethod, HandlerTask handlerTask, boolean refresh, SwipeRefreshLayout swipeRefreshLayout) {
        this.requestMethod = requestMethod;
        this.handlerTask = handlerTask;
        this.refresh = refresh;
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    // antes de consumir o web service
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // redireciona a tarefa
        handlerTask.onPreHandler();
    }

    // processa o consumo da API
    @Override
    protected String doInBackground(String... params) {
        String retorno = null;
        try {
            // endereço do web service
            String endereco = params[0];
            // Cria uma URL
            URL url = new URL(endereco);
            // cria uma conexão http por meio de uma URL
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // parametriza a conexão
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(15000);
            // define o método http a ser utilizado
            connection.setRequestMethod(requestMethod.toString());
            // processa a requisição de acordo com o método
            // se o método for POST ou PUT
            if (requestMethod == RequestMethod.POST || requestMethod == RequestMethod.PUT) {
                String json = params[1];
                // define propriedades da conexão
                connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                // habilita a escrita na requisição http
                connection.setDoOutput(true);
                // cria um outputstream
                OutputStream outputStream = connection.getOutputStream();
                // escreve o json (body) no outputstream
                outputStream.write(json.getBytes("UTF-8"));
                // libera o outputstream
                outputStream.flush();
                // fecha o outputstream
                outputStream.close();
            }
            // obtém o responsecode
            int responseCode = connection.getResponseCode();
            switch (requestMethod) {
                case GET:
                case POST:
                case PUT:
                    // verifica o responsecode
                    // se o response code for 201 ou 200
                    if (responseCode == HttpURLConnection.HTTP_CREATED || responseCode == HttpURLConnection.HTTP_OK) {
                        // recebe a resposta do servidor
                        InputStream inputStream = connection.getInputStream();
                        // a variável retorno recebe os dados do inputstream
                        // faz a leitura de um inputStream e retorna uma string para a variavel retorno
                        retorno = ReadStreamUtil.readStream(inputStream);
                        inputStream.close();
                    } else {
                        exception = new RuntimeException("Erro no " + requestMethod.toString() + " código de resposta: " + responseCode);
                    }
                    break;

                case DELETE:
                    // verifica o responde code
                    if (responseCode == HttpURLConnection.HTTP_NO_CONTENT) {
                        // ok
                        retorno = "";
                    } else {
                        exception = new  RuntimeException("Erro no " + requestMethod.toString() + " código de resposta: " + responseCode);
                    }
            }
            // fecha a conexão (desconecta do servidor)
            connection.disconnect();

        } catch (MalformedURLException e) {
            exception = new RuntimeException("Erro na URL: " + e.getMessage());
        } catch (ProtocolException e) {
            exception = new RuntimeException("Erro no protocolo: " + e.getMessage());
        } catch (IOException e) {
            exception = new RuntimeException("Erro ao acessar os dados: " + e.getMessage());
        } catch (IndexOutOfBoundsException e) {
            exception = new RuntimeException("Erro: falta um parâmetro para a execução do método. ");
        }
        // retorna a resposta do servidor
        return retorno;
    }

    @Override
    protected void onPostExecute(String s) {
        // Se tudo deu certo
        if (s != null) {
            // redireciona a resposta do servidor para o onSuccess
            handlerTask.onSuccess(s);
            // retira da tela o swiperefresh
            if (swipeRefreshLayout != null) {
                swipeRefreshLayout.setRefreshing(false);
            }
        } else {
            // se deu ruim
            handlerTask.onError(exception);
        }
    }

}
