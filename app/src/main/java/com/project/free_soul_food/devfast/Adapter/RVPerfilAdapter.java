package com.project.free_soul_food.devfast.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.project.free_soul_food.devfast.Model.Motoboy;
import com.project.free_soul_food.devfast.R;

public class RVPerfilAdapter extends RecyclerView.Adapter<RVPerfilAdapter.PerfilViewHolder> {
    private final Context context;
    private final Motoboy motoboy;
    final MotoboysOnClickListener onClickListener;

    public RVPerfilAdapter(Context context, Motoboy motoboy, MotoboysOnClickListener onClickListener) {
        this.context = context;
        this.motoboy = motoboy;
        this.onClickListener = onClickListener;
    }

    @Override
    public PerfilViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_perfil, parent, false);

        return new PerfilViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PerfilViewHolder holder, int position) {
        holder.nomeMotoboy.setText(motoboy.getNome());
        holder.emailMotoboy.setText(motoboy.getLogin().getEmail());
        holder.telefoneMotoboy.setText(motoboy.getTelefoneMotoboy());
        holder.cnhMotoboy.setText(motoboy.getCnh());
        holder.cpfMotoboy.setText(motoboy.getCpf());
        holder.enderecoMotoboy.setText(motoboy.getEndereco().getLogradouro());
        holder.marca.setText(motoboy.getMoto().getMarca());
        holder.placa.setText(motoboy.getMoto().getPlaca());
        holder.modeloMoto.setText(motoboy.getMoto().getModelo());
        holder.ano.setText(motoboy.getMoto().getAno());
        holder.cor.setText(motoboy.getMoto().getCor());
        if(onClickListener != null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClickPerfil(v);
                }
            });
        }
        if (motoboy.getFoto() != null){
            Bitmap bitmap = BitmapFactory.decodeFile(motoboy.getFoto());
            holder.imageMotoboy.setImageBitmap(bitmap);
        }else{
            holder.imageMotoboy.setImageResource(R.drawable.perfilicone);
        }


    }

    //interface do listener
    public interface  MotoboysOnClickListener{
        void onClickPerfil(View view);
    }


    @Override
    public int getItemCount() {
        return 1;
    }

   public class PerfilViewHolder extends RecyclerView.ViewHolder{

       RelativeLayout relativeLayout;
        ImageView imageMotoboy;
        TextView nomeMotoboy;
       // TextView entregasRecebidas;
        //TextView entregasFeitas;
       // TextView entregasNegadas;
        TextView cpfMotoboy;
        TextView cnhMotoboy;
        TextView emailMotoboy;
        TextView enderecoMotoboy;
        TextView telefoneMotoboy;
        TextView modeloMoto;
        TextView placa;
        TextView marca;
        TextView ano;
        TextView cor;

        View itemView;


        public PerfilViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;

            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relative_perfil);
            imageMotoboy = (ImageView) itemView.findViewById(R.id.foto_perfil_adapter);
            nomeMotoboy = (TextView) itemView.findViewById(R.id.nome_motoboy_adapter);
            cpfMotoboy = (TextView) itemView.findViewById(R.id.numero_cpf_adapter);
            cnhMotoboy =  (TextView) itemView.findViewById(R.id.numero_cnh_adapter);
            emailMotoboy = (TextView) itemView.findViewById(R.id.email_adapter);
            enderecoMotoboy = (TextView) itemView.findViewById(R.id.endereco_adapter);
            telefoneMotoboy = (TextView) itemView.findViewById(R.id.telefone_adapter);
            modeloMoto = itemView.findViewById(R.id.modelo_moto_adapter);
            placa = itemView.findViewById(R.id.placa_moto_adapter);
            marca = itemView.findViewById(R.id.marca_moto_adapter);
            ano = itemView.findViewById(R.id.ano_moto_adapter);
            cor = itemView.findViewById(R.id.cor_moto_adapter);

        }
    }
}
