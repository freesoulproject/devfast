package com.project.free_soul_food.devfast.task;

public interface HandlerTask {

    void onPreHandler();

    void onHandle();

    void onSuccess(String readValue);

    void onError(Exception erro);
}
