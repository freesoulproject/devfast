package com.project.free_soul_food.devfast.Enum;

/**
 * Created by Free_Soul_Food on 15/09/2017.
 */

public enum EstadoSolicitacao {
    ACEITO, RECUSADO;
}
