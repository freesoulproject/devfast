package com.project.free_soul_food.devfast.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.project.free_soul_food.devfast.Model.Moto;
import com.project.free_soul_food.devfast.Model.Motoboy;
import com.project.free_soul_food.devfast.R;
import com.project.free_soul_food.devfast.Rest.JsonParser;
import com.project.free_soul_food.devfast.Rest.RestAddress;
import com.project.free_soul_food.devfast.task.HandlerTask;
import com.project.free_soul_food.devfast.task.HandlerTaskAdapter;
import com.project.free_soul_food.devfast.task.TaskRest;

public class AlterarMotoActivity extends BaseActivity {
    EditText modelo, marca, placa, ano, cor;
    Moto moto;
    Motoboy motoboy;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alterar_moto);
        inicializarComponentes();
    }

    @Override
    public void inicializarComponentes() {

        motoboy = AppActivity.motoboy;
    }

    public void alterarMotoOnCLick(View view) {
        modelo = (EditText) findViewById(R.id.modelo_moto_edit);
        marca = (EditText) findViewById(R.id.marca_moto_edit);
        placa = (EditText) findViewById(R.id.placa_moto_edit);
        ano = (EditText) findViewById(R.id.ano_moto_edit);
        cor = (EditText) findViewById(R.id.cor_moto_edit);
        moto = new Moto();
        moto.setIdMoto(motoboy.getMoto().getIdMoto());
        moto.setModelo(modelo.getText().toString());
        moto.setMarca(marca.getText().toString());
        moto.setPlaca(placa.getText().toString());
        moto.setAno(ano.getText().toString());
        moto.setCor(cor.getText().toString());

        alterar(moto);




    }

    private void alterar(Moto moto) {
        JsonParser<Motoboy> parserMotoboy = null;

        String json = null;
        if (motoboy == null) {
            motoboy = new Motoboy();
        }

        motoboy.setMoto(moto);

        parserMotoboy = new JsonParser<>(Motoboy.class);
        json = parserMotoboy.fromObject(motoboy);

        new TaskRest(TaskRest.RequestMethod.POST, handlerAtualiza).execute(RestAddress.atualizaMoto, json);

    }

    HandlerTask handlerAtualiza = new HandlerTaskAdapter() {
        @Override
        public void onSuccess(String readValue) {
            super.onSuccess(readValue);
            JsonParser<Motoboy> parserMotoboy= new JsonParser<>(Motoboy.class);
            motoboy = parserMotoboy.toObject(readValue);
            Intent intent = new Intent(AlterarMotoActivity.this, AppActivity.class);
            intent.putExtra("motoboy",motoboy);
            startActivity(intent);

            Toast.makeText(AlterarMotoActivity.this, "atualizou", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(Exception erro) {
            super.onError(erro);
            Toast.makeText(AlterarMotoActivity.this, "Erro ao atualizar", Toast.LENGTH_SHORT).show();
        }
    };


}
