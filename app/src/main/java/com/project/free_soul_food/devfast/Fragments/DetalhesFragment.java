package com.project.free_soul_food.devfast.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.free_soul_food.devfast.Adapter.RVDetalhesAdapter;
import com.project.free_soul_food.devfast.Model.Entrega;
import com.project.free_soul_food.devfast.R;
import com.project.free_soul_food.devfast.Rest.JsonParser;
import com.project.free_soul_food.devfast.Rest.RestAddress;
import com.project.free_soul_food.devfast.task.HandlerTask;
import com.project.free_soul_food.devfast.task.HandlerTaskAdapter;
import com.project.free_soul_food.devfast.task.TaskRest;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetalhesFragment extends Fragment {
    View view;

    RecyclerView recyclerDetalhes;
    List<Entrega> entregas = new ArrayList<>();
    Entrega entrega;
    SwipeRefreshLayout swipeRefreshDetalhes;


    public DetalhesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       view= inflater.inflate(R.layout.fragment_detalhes, container, false);
        createSwipeRefresh();
        getDetalhes();
        return view;
    }

    // cria o recyclerview
    private void createRecyclerView(List<Entrega> entregas) {
        recyclerDetalhes = view.findViewById(R.id.recycler_detalhes);
        recyclerDetalhes.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerDetalhes.setHasFixedSize(true);
        // instancia o adaptador
        RVDetalhesAdapter adapter = new RVDetalhesAdapter(entregas,getContext(),onClickEntrega());
        recyclerDetalhes.setAdapter(adapter);
    }

    private RVDetalhesAdapter.DetalhesOnClickListener onClickEntrega() {
        return new RVDetalhesAdapter.DetalhesOnClickListener() {
            @Override
            public void onClickDetalhes(View view, int position) {
                // Recupera uma entrega da lista
                entrega = entregas.get(position);
                // registrar o menu de context no recyclerview
                registerForContextMenu(recyclerDetalhes);
                // abre o menu de contexto
                getActivity().openContextMenu(view);
            }
        };
    }

    // executa a asynctask
    private void getDetalhes() {
        // executa a asynctask
        new TaskRest(TaskRest.RequestMethod.GET, handlerListar, true, createSwipeRefresh()).execute(RestAddress.listaEntrega);
    }

    private SwipeRefreshLayout createSwipeRefresh() {
        swipeRefreshDetalhes = view.findViewById(R.id.swipe_detalhes);
        // associa um listener ao swipeRefresh
        swipeRefreshDetalhes.setOnRefreshListener(onRefreshEntrega());
        return this.swipeRefreshDetalhes;
    }

    // listener para o swipeRefresh
    private SwipeRefreshLayout.OnRefreshListener onRefreshEntrega() {
        return new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDetalhes();
            }
        };
    }

    // handler
    private HandlerTask handlerListar = new HandlerTaskAdapter() {
        @Override
        public void onSuccess(String readValue) {
            super.onSuccess(readValue);
            // cria uma instancia de jsonparser
            JsonParser<Entrega> parserEntrega = new JsonParser<>(Entrega.class);
            // inicializa a lista de entregas convertendo json[] para list<Cliente>
            entregas = parserEntrega.toList(readValue, Entrega[].class);
            // passa a lista de entregas para criar o recyclerview
            createRecyclerView(entregas);
        }

        @Override
        public void onError(Exception erro) {
            super.onError(erro);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        // Executa a task de consulta GET
        getDetalhes();
    }


}
