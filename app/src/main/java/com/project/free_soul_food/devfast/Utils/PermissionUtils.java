package com.project.free_soul_food.devfast.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Free Soul Food on 28/09/2017.
 */

public class PermissionUtils {

    public static  boolean isValidate(Activity activity, int requestCode, String... permissoes ){
        List<String> list = new ArrayList<>();

        for (String permission : permissoes){
            //verifica se a primeira permissao ainda não foi solicitada
            if (!checkPermissions (activity,permission)){
                list.add(permission);
            }
        }
        //se a lista estiver vazio nao há mais permissoes para verificar
        if (list.isEmpty()){
            return true;
        }
        //se houver elementos na lista converte o arrayList em Array
        String[] newPermissions = new String[list.size()];
        list.toArray(newPermissions);
        //e solicita a permissao para o usuário
        ActivityCompat.requestPermissions(activity, newPermissions, requestCode);
        //retorna falso pois ainda podem existir permissoes
        return false;
    }

    private static boolean checkPermissions(Context context, String permission){
        return ContextCompat.checkSelfPermission(context,permission) == PackageManager.PERMISSION_GRANTED;
    }
}


