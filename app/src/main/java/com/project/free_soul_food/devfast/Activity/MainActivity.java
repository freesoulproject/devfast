package com.project.free_soul_food.devfast.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.project.free_soul_food.devfast.Model.Login;
import com.project.free_soul_food.devfast.Model.Motoboy;
import com.project.free_soul_food.devfast.R;
import com.project.free_soul_food.devfast.Rest.JsonParser;
import com.project.free_soul_food.devfast.Rest.RestAddress;
import com.project.free_soul_food.devfast.Utils.FormValidatorUtil;
import com.project.free_soul_food.devfast.Utils.TextWatcherUtil;
import com.project.free_soul_food.devfast.task.HandlerTask;
import com.project.free_soul_food.devfast.task.HandlerTaskAdapter;
import com.project.free_soul_food.devfast.task.TaskRest;

import org.parceler.Parcels;

public class MainActivity extends BaseActivity {
    Login l;
    public Motoboy motoboy;

    TextInputEditText editEmail;
    TextInputLayout layoutEmail;
    TextInputEditText editSenha;
    TextInputLayout layoutSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inicializarComponentes();
    }

    @Override
    public void inicializarComponentes() {
        editEmail = (TextInputEditText) findViewById(R.id.input_email);
        layoutEmail = (TextInputLayout) findViewById(R.id.input_email_layout);
        editSenha = (TextInputEditText) findViewById(R.id.input_senha);
        layoutSenha = (TextInputLayout) findViewById(R.id.input_senha_layout);

        editEmail.addTextChangedListener(new TextWatcherUtil(this, layoutEmail, editEmail));
        editSenha.addTextChangedListener(new TextWatcherUtil(this, layoutSenha, editSenha));
    }

    public void loginOnClick(View view) {
        if (isFormvalid()) {
            login();
        }

    }

    public void esqueciOnCLick(View view) {
        Intent intent = new Intent(this, SenhaActivity.class);
        startActivity(intent);
    }

    private boolean isFormvalid() {
        // Valida o email
        if (!FormValidatorUtil.isValidEmail(editEmail.getEditableText().toString())) {
            layoutEmail.setError(getString(R.string.email_error_msg));
            editEmail.requestFocus();
            return false;
        } else if (!FormValidatorUtil.isValidPassword(editSenha.getEditableText().toString())) {
            layoutSenha.setError(getString(R.string.senha_error_msg));
            editSenha.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    private void login() {
        JsonParser<Login> jsonParser;
        String json;
        try {

            if (l == null) {
                l = new Login();
            }

            l.setEmail(editEmail.getEditableText().toString());

            l.setSenha(editSenha.getEditableText().toString());

            jsonParser = new JsonParser<>(Login.class);
            json = jsonParser.fromObject(l);


          new TaskRest(TaskRest.RequestMethod.POST, handlerLogar).execute(RestAddress.logar, json);

        } catch (RuntimeException e) {
            //handlerLogar.onError(e);
            //e.getCause();
            //Toast.makeText(this, "ENTROU AQUI", Toast.LENGTH_SHORT).show();
        }


        // pesquisa pra verificar se o email existe
        // se existir, faz o post
        // senao vc manda uma mensagem


    }

    private HandlerTask handlerLogar = new HandlerTaskAdapter() {

        @Override
        public void onHandle() {
            super.onHandle();
        }

        @Override
        public void onSuccess(String readValue) {
            super.onSuccess(readValue);
            JsonParser<Motoboy> parserMotoboy= new JsonParser<>(Motoboy.class);
            motoboy = parserMotoboy.toObject(readValue);
            
           /* if( motoboy.getStatusMotoboy().equals("Desativado")){
                Toast.makeText(MainActivity.this, "Usuário Desativado", Toast.LENGTH_SHORT).show();

            }else */
           if(readValue.equals("Usuário ou senha INVÁLIDOS")) {
                Toast.makeText(MainActivity.this, getString(R.string.erro_login), Toast.LENGTH_SHORT).show();
            } else {

                fazLogin(motoboy);


            }

        }

        @Override
        public void onError(Exception erro) {
            super.onError(erro);
            Toast.makeText(MainActivity.this, getString(R.string.erro_login), Toast.LENGTH_SHORT).show();
        }

    };

    private void fazLogin(Motoboy motoboy) {
        // Intent intent = new Intent(this, AppActivity.class);
       // SharedPreferences sharedPreferences= getSharedPreferences("Motoboy", Context.MODE_PRIVATE);
       // SharedPreferences.Editor editor = sharedPreferences.edit();
       // editor.putString("nomeMotoboy",motoboy.getNome());
        //editor.commit();
        /*Bundle b = new Bundle();
        b.putParcelable("motoboy",Parcels.wrap(motoboy) );*/
       // intent.putExtra("motoboy",Parcels.wrap(motoboy));
        // startActivity(intent);
       // Bundle bundle = new Bundle();
        //bundle.putParcelable("motoboy", Parcels.wrap(motoboy));
        Intent i = new Intent(this, AppActivity.class);
        i.putExtra("motoboy", motoboy);
        startActivity(i);
    }



}
