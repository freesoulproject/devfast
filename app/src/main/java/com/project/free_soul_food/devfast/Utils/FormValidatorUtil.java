package com.project.free_soul_food.devfast.Utils;

import android.text.TextUtils;
import android.util.Patterns;

public class FormValidatorUtil {

    /**
     * Verifica se o email está em branco e se o texto do campo é um e-mail
     * @param email email do usuário
     * @return o resultado da verificação
     */
    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /**
     * Verifica se a senha não esta em branco e se a senha tem 8 caracteres
     * @param password senha
     * @return resultado da verificação
     */
    public static boolean isValidPassword(String password) {
        return !TextUtils.isEmpty(password) && password.trim().length() <= 8;
    }
}
