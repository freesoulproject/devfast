package com.project.free_soul_food.devfast.Model;

/**
 * Created by Free_Soul_Food on 15/09/2017.
 */

public class Administrador {
    private Long idAdministrador;
    private Login login;

    public Long getIdAdministrador() {
        return idAdministrador;
    }

    public void setIdAdministrador(Long idAdministrador) {
        this.idAdministrador = idAdministrador;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }
}
