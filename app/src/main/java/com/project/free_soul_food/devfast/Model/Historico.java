package com.project.free_soul_food.devfast.Model;

import com.project.free_soul_food.devfast.Enum.EstadoSolicitacao;

import java.util.Date;

/**
 * Created by Free_Soul_Food on 15/09/2017.
 */

public class Historico {
    private Long idHistorico;
    private Date inicio;
    private Date fim;
    private EstadoSolicitacao estadoSolicitacao;
    private Entrega entrega;
    private Motoboy motoboy;

    public Long getIdHistorico() {
        return idHistorico;
    }

    public void setIdHistorico(Long idHistorico) {
        this.idHistorico = idHistorico;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFim() {
        return fim;
    }

    public void setFim(Date fim) {
        this.fim = fim;
    }

    public EstadoSolicitacao getEstadoSolicitacao() {
        return estadoSolicitacao;
    }

    public void setEstadoSolicitacao(EstadoSolicitacao estadoSolicitacao) {
        this.estadoSolicitacao = estadoSolicitacao;
    }

    public Entrega getEntrega() {
        return entrega;
    }

    public void setEntrega(Entrega entrega) {
        this.entrega = entrega;
    }

    public Motoboy getMotoboy() {
        return motoboy;
    }

    public void setMotoboy(Motoboy motoboy) {
        this.motoboy = motoboy;
    }
}
