package com.project.free_soul_food.devfast.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.project.free_soul_food.devfast.Fragments.EntregasFragment;
import com.project.free_soul_food.devfast.Fragments.MotoboyFragment;
import com.project.free_soul_food.devfast.Model.Moto;
import com.project.free_soul_food.devfast.Model.Motoboy;
import com.project.free_soul_food.devfast.R;
import com.project.free_soul_food.devfast.Adapter.TabAdapter;
import com.project.free_soul_food.devfast.Rest.JsonParser;
import com.project.free_soul_food.devfast.Rest.RestAddress;
import com.project.free_soul_food.devfast.Utils.PermissionUtils;
import com.project.free_soul_food.devfast.task.HandlerTask;
import com.project.free_soul_food.devfast.task.HandlerTaskAdapter;
import com.project.free_soul_food.devfast.task.TaskRest;

import java.io.File;
import java.util.List;

public class AppActivity extends BaseActivity {
    TabLayout tabLayout;
    public static Motoboy motoboy;
    ViewPager viewPager;
    public static final int CAMERA_REQUEST_CODE = 1;
    public static final int GALERIA_REQUEST_CODE = 2;
    String[] permissoes = new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    File fotoFile;



    ImageView imgMotoboy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);
        motoboy = (Motoboy) getIntent().getSerializableExtra("motoboy");
        inicializarComponentes();

        //SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("helena", Context.MODE_PRIVATE);
        //Log.d("token", sharedpreferences.getString("token", null));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        MenuItem itemSwitch = menu.findItem(R.id.mySwitch);
        itemSwitch.setActionView(R.layout.switch_layout);
        final SwitchCompat aSwitch = menu.findItem(R.id.mySwitch).getActionView().findViewById(R.id.switch_action);
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    Toast.makeText(AppActivity.this, "Switch On", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AppActivity.this, "Switch Off", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public void inicializarComponentes() {
        setupTabs();
        setupViewPager();



    }

    private void setupTabs() {
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        //adicionar as tabs
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.map));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.entregas));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.profile));
        //preenche completamente o espaco do tablayout
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        int cor = ContextCompat.getColor(this, R.color.branco);
        tabLayout.setTabTextColors(cor, cor);

        //listener das tabs
        tabLayout.addOnTabSelectedListener(onTabSelected());


    }

    private TabLayout.OnTabSelectedListener onTabSelected() {
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }

    private void setupViewPager() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        //associa o view pager ao adaptador
        viewPager.setAdapter(new TabAdapter(getSupportFragmentManager(), tabLayout.getTabCount()));
        //associa um listerne ao viewpager
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    public void infoOnClick(View view) {

        Intent intent = new Intent(this, DetalhesActivity.class);
        startActivity(intent);

    }

    public void alterarOnClick(View view) {
        Intent intent = new Intent(this, AlterarMotoActivity.class);
        startActivity(intent);
    }
    public void onClickImg(View view) {
        imgMotoboy = (ImageView) findViewById(R.id.foto_perfil_adapter);
        opcoesDialog().show();
    }

    public AlertDialog opcoesDialog() {
        CharSequence[] opcoes = new CharSequence[]{getString(R.string.editFoto), getString(R.string.excluirFoto)};
        //constoi o alert dialog
        AlertDialog.Builder builer = new AlertDialog.Builder(this);
        builer.setTitle(getString(R.string.opc));
        builer.setItems(opcoes, onOpSelectItem());

        return builer.create();
    }

    private DialogInterface.OnClickListener onOpSelectItem() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int id) {
                switch (id) {
                    case 0:
                        //editar foto
                        fotoDialog().show();
                        break;
                    case 1:
                        //excluir foto
                        //removeDialog().show();
                        break;
                }
            }
        };
    }
    public AlertDialog fotoDialog() {
        CharSequence[] opcoes = new CharSequence[]{getString(R.string.camera), getString(R.string.galeria)};
        //constoi o alert dialog
        AlertDialog.Builder builer = new AlertDialog.Builder(this);
        builer.setTitle(getString(R.string.select));
        builer.setItems(opcoes, onFotoSelectItem());

        return builer.create();
    }
    private DialogInterface.OnClickListener onFotoSelectItem() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        //camera
                        disparaCamera();
                        //Toast.makeText(AppActivity.this, "camera", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        //galeria
                        if (PermissionUtils.isValidate(AppActivity.this, GALERIA_REQUEST_CODE, permissoes)) {
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent, GALERIA_REQUEST_CODE);
                        }
                        Toast.makeText(AppActivity.this, "galeria", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };
    }
    public void disparaCamera() {
        //verifica as permissoes
        if (PermissionUtils.isValidate(this, CAMERA_REQUEST_CODE, permissoes)) {
            //acessar a camera
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            //Alguma forma para executar a intent
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                //pepara o arquivo de foto
                fotoFile = null;
                fotoFile = createImage();
                if (fotoFile != null) {
                    //determina o caminho para salvar a foto
                    Uri uri = FileProvider.getUriForFile(this, "com.example.android.devfast", fotoFile);
                    //Inicia a activitu para exibir a foto no imageView
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);

                }
            }
        }
    }
    public File createImage() {
        String name = System.currentTimeMillis() + ".jpg";
        File diretorio = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        String caminho = diretorio.getPath() + "/" + name;
        File imageFile = new File(caminho);

        return imageFile;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bitmap = null;

        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            bitmap = BitmapFactory.decodeFile(fotoFile.getAbsolutePath());

        }
        if (requestCode == GALERIA_REQUEST_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
                //nome do diretorio
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                //passa para um cursosr o caminho onde esta a imagem
                Cursor cursor = this.getContentResolver().query(uri, filePathColumn, null, null, null);
                //move o cursor para a primeira posicao
                cursor.moveToFirst();
                //obtem o id da coluna 0
                //caminho da imagem
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String caminho = cursor.getString(columnIndex);
                //fecha cursor
                cursor.close();
                //cria um file com o caminho selecionado
                fotoFile = new File(caminho);

                bitmap = BitmapFactory.decodeFile(fotoFile.getAbsolutePath());
            }
        }
        //coloca a foto no imageView
        imgMotoboy.setImageBitmap(bitmap);
       // confirmarAltera().show();

    }

    //alert Dialog de confirmação
    private AlertDialog confirmarAltera() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.titulo_confirma))
                .setMessage(getString(R.string.msg_confirma))
                .setPositiveButton(getString(R.string.sim), onSaveMotoboy())
                .setNegativeButton(getString(R.string.nao), onSaveMotoboy());
        return builder.create();
    }

    private DialogInterface.OnClickListener onSaveMotoboy() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //sim
                        Toast.makeText(AppActivity.this, "SIM", Toast.LENGTH_SHORT).show();
                        JsonParser<Motoboy> parserCliente;
                        String motoboyJson;

                        if (motoboy == null) {
                            motoboy = new Motoboy();
                        }

                        if (motoboy.getIdMotoboy() != null) {
                            // update
                            // converte o objeto cliente em json
                            parserCliente = new JsonParser<>(Motoboy.class);
                            motoboyJson = parserCliente.fromObject(motoboy);
                            // executa a task de update (PUT)
                            new TaskRest(TaskRest.RequestMethod.PUT, handlerSalvar).execute(String.format(RestAddress.atualizaMotoboy, motoboy.getIdMotoboy()), motoboyJson);
                        } else {
                      //      Toast.makeText(this, "Não foi possivel adicionar sua foto", Toast.LENGTH_SHORT).show();
                            Bitmap bm = BitmapFactory.decodeResource(AppActivity.this.getResources(), R.drawable.perfilicone);
                            imgMotoboy.setImageBitmap(bm);
                        }
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //não
                        dialogInterface.dismiss();
                        break;
                }
            }
        };
    }

    private HandlerTask handlerSalvar = new HandlerTaskAdapter() {
        @Override
        public void onSuccess(String readValue) {
            super.onSuccess(readValue);
            Toast.makeText(AppActivity.this, "Foto Salva com sucesso", Toast.LENGTH_SHORT).show();
           //Toast.makeText(this, "Foto Salva com sucesso", Toast.LENGTH_SHORT).show();
            //clear();
        }

        @Override
        public void onError(Exception erro) {
            super.onError(erro);
           // Toast.makeText(this, erro.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    private void clear() {
        Bitmap bm = BitmapFactory.decodeResource(AppActivity.this.getResources(), R.drawable.perfilicone);
        imgMotoboy.setImageBitmap(bm);

    }


}
