package com.project.free_soul_food.devfast.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.project.free_soul_food.devfast.Enum.NivelFragilidade;
import com.project.free_soul_food.devfast.Model.Entrega;
import com.project.free_soul_food.devfast.R;

import java.util.List;

/**
 * Created by Free_Soul_Food on 29/09/2017.
 */

public class RVDetalhesAdapter extends RecyclerView.Adapter<RVDetalhesAdapter.DetalhesViewHolder> {
    private final List<Entrega> entregas;
    private final Context context;
    final DetalhesOnClickListener onClickDetalhes;

    public RVDetalhesAdapter(List<Entrega> entregas, Context context, DetalhesOnClickListener onClickListener) {
        super();
        this.entregas = entregas;
        this.onClickDetalhes = onClickListener;
        this.context = context;
    }

    // interface de listener
    public interface DetalhesOnClickListener {
        void onClickDetalhes(View view, int position);
    }


    public DetalhesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_detalhes, parent, false);

        return new DetalhesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DetalhesViewHolder holder, final int position) {

        holder.nomeCliente.setText(entregas.get(position).getNomeCliente());
        holder.telefoneCliente.setText(entregas.get(position).getTelefoneCliente());
        holder.fragilidadeEntrega.setText(entregas.get(position).getFragilidade().toString());
        holder.tamanhoEntrega.setText(entregas.get(position).getTamanho().toString());
        holder.totalEntrega.setText(entregas.get(position).getValorTotal().toString());
        holder.enderecoCliente.setText(entregas.get(position).getEndereco().getLogradouro());
        holder.complemento.setText(entregas.get(position).getEndereco().getComplemento());
        holder.estado.setText(entregas.get(position).getEndereco().getEstado());
        holder.numeroEndCliente.setText(String.valueOf(entregas.get(position).getEndereco().getNumero()));
        holder.descricaoEntrega.setText(entregas.get(position).getDescricao());
        holder.bairroEntrega.setText(entregas.get(position).getEndereco().getBairro());
//        holder.nomeRestaurante.setText(entregas.get(position).getRestaurante().getNome());

        if (onClickDetalhes != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickDetalhes.onClickDetalhes(view, position);
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return entregas != null ? entregas.size() : 0;
    }

    public class DetalhesViewHolder extends RecyclerView.ViewHolder {
        //viewsa
        CardView cardDetalhes;
        TextView nomeCliente;
        TextView telefoneCliente;
        TextView enderecoCliente;
        TextView numeroEndCliente;
        TextView complemento;
        TextView estado;
        TextView totalEntrega;
        TextView nomeRestaurante;
        TextView bairroEntrega;
        TextView descricaoEntrega;
        TextView fragilidadeEntrega;
        TextView tamanhoEntrega;
        View itemView;


        public DetalhesViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            //faz o findviewbyid

            cardDetalhes = itemView.findViewById(R.id.card_detalhes);

            nomeCliente = itemView.findViewById(R.id.nome_cliente);
            nomeRestaurante = itemView.findViewById(R.id.nome_restaurante);
            bairroEntrega = itemView.findViewById(R.id.bairro_cliente_entrega);
            descricaoEntrega = itemView.findViewById(R.id.descricao_pedido_entrega);
            telefoneCliente = itemView.findViewById(R.id.telefone_cliente_entrega);
            enderecoCliente = itemView.findViewById(R.id.endereco_cliente_entrega);
            numeroEndCliente = itemView.findViewById(R.id.numero_endereco_cliente_entrega);
            complemento = itemView.findViewById(R.id.complemento_cliente_entrega);
            estado = itemView.findViewById(R.id.estado_cliente_entrega);
            totalEntrega = itemView.findViewById(R.id.total_pedido_entrega);
            fragilidadeEntrega = itemView.findViewById(R.id.fragilidade_pedido_entrega);
            tamanhoEntrega = itemView.findViewById(R.id.tamanho_pedido_entrega);


        }
    }
}
