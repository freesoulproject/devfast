package com.project.free_soul_food.devfast.Fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;


import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.project.free_soul_food.devfast.Model.Endereco;
import com.project.free_soul_food.devfast.Model.Entrega;
import com.project.free_soul_food.devfast.R;
import com.project.free_soul_food.devfast.Rest.JsonParser;
import com.project.free_soul_food.devfast.Rest.RestAddress;
import com.project.free_soul_food.devfast.task.HandlerTask;
import com.project.free_soul_food.devfast.task.HandlerTaskAdapter;
import com.project.free_soul_food.devfast.task.MapTaskRest;
import com.project.free_soul_food.devfast.task.TaskRest;


import android.widget.Toast;

import org.apache.http.params.HttpConnectionParams;

import java.io.IOException;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapaFragment extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    View view;
    private GoogleMap googleMap;
    private GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    public List<String> entregasAceitas;
    ArrayList<LatLng> MarkerPoints;
    public List<Entrega> entregas = new ArrayList<>();

    LatLng origem = new LatLng(-25.443195, -49.280977);
    LatLng destino =new LatLng( -25.442207, -49.278403);
    public MapaFragment() {
        // Required empty public constructor
    }

    private void getEntrega() {
        // executa a asynctask
        new TaskRest(TaskRest.RequestMethod.GET, handlerListar).execute(RestAddress.listaEntrega);
    }

    private HandlerTask handlerListar = new HandlerTaskAdapter() {
        @Override
        public void onSuccess(String readValue) {
            super.onSuccess(readValue);
            // cria uma instancia de jsonparser
            JsonParser<Entrega> parserEntrega = new JsonParser<>(Entrega.class);
            // inicializa a lista de entregas convertendo json[] para list<Entrega>
            entregas = parserEntrega.toList(readValue, Entrega[].class);

            for (int i = 0; i <= entregas.size() - 1; i++) {
                //  entregasAceitas.add(entregas.get(i).getEndereco().getLogradouro());
            }
        }

        @Override
        public void onError(Exception erro) {
            super.onError(erro);
        }
    };



        /*try {
            getLatLng();
        } catch (IOException e) {
            e.printStackTrace();
        }*/


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_mapa, container, false);
        MarkerPoints = new ArrayList<>();

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);//10 segundos
        mLocationRequest.setFastestInterval(5000); // 5 segundos

        //Precisão do GPS
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
       // getEntrega();



        mapFragment.getMapAsync(this);
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    private void getLatLng() throws IOException {
        String endereco = entregasAceitas.get(1);
        Geocoder gc = new Geocoder(getContext(), new Locale("pt", "BR"));
        List<Address> list = gc.getFromLocationName(endereco, 10);
        Toast.makeText(getContext(), "Retorno:" + list, Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onPause() {
        super.onPause();
//        stopLocationUpdates();//Para o gps
    }

    @Override
    public void onStop() {
        stopLocationUpdates();
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {

        //Conecta ao google play services
        startLocationRequest();
        Toast.makeText(getContext(), "Conectado ao Google Services", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(getContext(), "Conexão Interrompida", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(getContext(), "Erro ao se conectar", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onLocationChanged(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        double rota = latitude - longitude;
        Log.d("map", "onLocationChanged:" + location);
        setMapLocation(location);
    }

    private void getLastLocation() {
        Location l = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        Log.d("map", "getLastLocation: " + l);
        setMapLocation(l);
    }

    private void setMapLocation(Location l) {
        if (googleMap != null && l != null) {
            LatLng ll = new LatLng(l.getLatitude(), l.getLongitude());
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 15);
            googleMap.animateCamera(update);
            Log.d("map", "setMapLocation:" + l);
            CircleOptions circle = new CircleOptions().center(ll);
            circle.fillColor(Color.BLUE);
            circle.radius(25);
            googleMap.clear();
            googleMap.addCircle(circle);
        }
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            googleMap.setMyLocationEnabled(true);
            getLastLocation();
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        } else {
            Toast.makeText(getContext(), "Não é possivel exibir sua Localização. ", Toast.LENGTH_SHORT).show();
        }
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {

                // Already two locations
                if (MarkerPoints.size() > 1) {
                    MarkerPoints.clear();
                    googleMap.clear();
                }

                // Adding new item to the ArrayList
                MarkerPoints.add(point);

                // Creating MarkerOptions
                MarkerOptions options = new MarkerOptions();

                // Setting the position of the marker
                options.position(point);

                /**
                 * For the start location, the color of marker is GREEN and
                 * for the end location, the color of marker is RED.
                 */
                if (MarkerPoints.size() == 1) {
                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                } else if (MarkerPoints.size() == 2) {
                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                }


                // Add new marker to the Google Map Android API V2
                googleMap.addMarker(options);

                // Checks, whether start and end locations are captured
                if (MarkerPoints.size() >= 2) {
                    LatLng origin = MarkerPoints.get(0);
                    LatLng dest = MarkerPoints.get(1);

                    // Getting URL to the Google Directions API
                    String url = getUrl(origin, dest);
                    Log.d("onMapClick", url.toString());
                    MapTaskRest FetchUrl = new MapTaskRest(googleMap);

                    // Start downloading json data from Google Directions API
                    FetchUrl.execute(url);
                    //move map camera
                   googleMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
                   googleMap.animateCamera(CameraUpdateFactory.zoomTo(11));
                }

            }
        });



    }
    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    protected void startLocationRequest() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        Log.d("map", "connectado");

    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }


}

