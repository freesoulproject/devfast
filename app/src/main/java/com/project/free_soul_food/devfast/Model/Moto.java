package com.project.free_soul_food.devfast.Model;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by Free_Soul_Food on 14/09/2017.
*/

public class Moto implements Serializable{
    private Long idMoto;
    private String ano;
    private String modelo;
    private String marca;
    private String cor;
    private String placa;

    public Long getIdMoto() {
        return idMoto;
    }

    public void setIdMoto(Long idMoto) {
        this.idMoto = idMoto;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }
}
