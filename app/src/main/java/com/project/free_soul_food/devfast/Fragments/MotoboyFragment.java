package com.project.free_soul_food.devfast.Fragments;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.project.free_soul_food.devfast.Activity.AlterarMotoActivity;
import com.project.free_soul_food.devfast.Activity.AppActivity;
import com.project.free_soul_food.devfast.Adapter.RVPerfilAdapter;
import com.project.free_soul_food.devfast.Model.Motoboy;
import com.project.free_soul_food.devfast.R;
import com.project.free_soul_food.devfast.Rest.JsonParser;
import com.project.free_soul_food.devfast.Rest.RestAddress;
import com.project.free_soul_food.devfast.Utils.PermissionUtils;
import com.project.free_soul_food.devfast.task.HandlerTask;
import com.project.free_soul_food.devfast.task.HandlerTaskAdapter;
import com.project.free_soul_food.devfast.task.TaskRest;

import java.io.File;


/**
 * A simple {@link Fragment} subclass.
 */
public class MotoboyFragment extends Fragment {


    View view;

    Button btnAlterar;
    RecyclerView recyclerMotoboy;
    SwipeRefreshLayout swipeRefreshMotoboy;
    Motoboy motoboy;


    public MotoboyFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_motoboy, container, false);

        // btnAlterar = (Button) view.findViewById(R.id.btnAlterar);
        // imgMotoboy = (ImageView) view.findViewById(R.id.foto_perfil_adapter);


        //createSwipeRefresh();
        getMotoboy();

        return view;
    }

    private void createRecyclerView(Motoboy motoboy) {
        recyclerMotoboy = view.findViewById(R.id.recycler_motoboy);
        recyclerMotoboy.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerMotoboy.setHasFixedSize(true);
        // instancia o adaptador
        RVPerfilAdapter adapter = new RVPerfilAdapter(getContext(), motoboy, onClickMotoboy());
        recyclerMotoboy.setAdapter(adapter);
    }

    private RVPerfilAdapter.MotoboysOnClickListener onClickMotoboy() {
        return new RVPerfilAdapter.MotoboysOnClickListener() {
            @Override
            public void onClickPerfil(View view) {
                motoboy = AppActivity.motoboy;
                //registra o menu de contexto no recycler
                registerForContextMenu(recyclerMotoboy);
                // abre o menu de contexto
                getActivity().openContextMenu(view);
            }
        };
    }

    private void getMotoboy() {
        // SharedPreferences preferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        //motoboy.setNome(getResources().getString("nomeMotoboy"));
        motoboy = AppActivity.motoboy;
        createRecyclerView(motoboy);
    }

    /*private SwipeRefreshLayout createSwipeRefresh() {
        swipeRefreshMotoboy = view.findViewById(R.id.swipe_motoboy);
        // associa um listener ao swipeRefresh
        swipeRefreshMotoboy.setOnRefreshListener(onMotoboyRefresh());
        return this.swipeRefreshMotoboy;
    }

    private SwipeRefreshLayout.OnRefreshListener onMotoboyRefresh() {
        return new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
            getMotoboy();
            }
        };
    }*/
    /*public void onClickImg(View view) {
        opcoesDialog().show();
    }

    public AlertDialog opcoesDialog() {
        CharSequence[] opcoes = new CharSequence[]{getString(R.string.editFoto), getString(R.string.excluirFoto)};
        //constoi o alert dialog
        AlertDialog.Builder builer = new AlertDialog.Builder(getContext());
        builer.setTitle(getString(R.string.opc));
        builer.setItems(opcoes, onOpSelectItem());

        return builer.create();
    }

    private DialogInterface.OnClickListener onOpSelectItem() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int id) {
                switch (id) {
                    case 0:
                        //editar foto
                        fotoDialog().show();
                        break;
                    case 1:
                        //excluir foto
                        removeDialog().show();
                        break;
                }
            }
        };
    }*/

   /* public AlertDialog removeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.titulo_remove))
                .setMessage(getString(R.string.msg_remove))
                .setPositiveButton(getString(R.string.sim), onRemoveFoto())
                .setNegativeButton(getString(R.string.nao), onRemoveFoto());
        return builder.create();
    }*/

   /* private DialogInterface.OnClickListener onRemoveFoto() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //sim
                        removeFoto();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //não
                        dialogInterface.dismiss();
                        break;
                }
            }
        };
    }

    public void removeFoto() {
        Bitmap bm = BitmapFactory.decodeResource(this.getResources(), R.drawable.perfilicone);
        imgMotoboy.setImageBitmap(bm);
        confirmarAltera().show();
    }

    public AlertDialog fotoDialog() {
        CharSequence[] opcoes = new CharSequence[]{getString(R.string.camera), getString(R.string.galeria)};
        //constoi o alert dialog
        AlertDialog.Builder builer = new AlertDialog.Builder(getContext());
        builer.setTitle(getString(R.string.select));
        builer.setItems(opcoes, onFotoSelectItem());

        return builer.create();
    }

    private DialogInterface.OnClickListener onFotoSelectItem() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        //camera
                        disparaCamera();
                        break;
                    case 1:
                        //galeria
                        if (PermissionUtils.isValidate(getActivity(), GALERIA_REQUEST_CODE, permissoes)) {
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent, GALERIA_REQUEST_CODE);
                        }
                        break;
                }
            }
        };
    }

    public File createImage() {
        String name = System.currentTimeMillis() + ".jpg";
        File diretorio = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        String caminho = diretorio.getPath() + "/" + name;
        File imageFile = new File(caminho);

        return imageFile;
    }

    public void disparaCamera() {
        //verifica as permissoes
        if (PermissionUtils.isValidate(getActivity(), CAMERA_REQUEST_CODE, permissoes)) {
            //acessar a camera
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            //Alguma forma para executar a intent

            if (takePictureIntent.resolveActivity(getActivity().getApplicationContext().getPackageManager()) != null) {
                //pepara o arquivo de foto
                fotoFile = null;
                fotoFile = createImage();
                if (fotoFile != null) {
                    //determina o caminho para salvar a foto
                    Uri uri = FileProvider.getUriForFile(getContext(), "com.example.android.devfast", fotoFile);
                    //Inicia a activitu para exibir a foto no imageView
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);

                }
            }
        }
    }*/

   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bitmap = null;

        if (requestCode == CAMERA_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            bitmap = BitmapFactory.decodeFile(fotoFile.getAbsolutePath());
        }
        if (requestCode == GALERIA_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
                //nome do diretorio
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                //passa para um cursosr o caminho onde esta a imagem
                Cursor cursor = getActivity().getContentResolver().query(uri, filePathColumn, null, null, null);
                //move o cursor para a primeira posicao
                cursor.moveToFirst();
                //obtem o id da coluna 0
                //caminho da imagem
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String caminho = cursor.getString(columnIndex);
                //fecha cursor
                cursor.close();
                //cria um file com o caminho selecionado
                fotoFile = new File(caminho);

                bitmap = BitmapFactory.decodeFile(fotoFile.getAbsolutePath());
            }
        }
        //coloca a foto no imageView
        imgMotoboy.setImageBitmap(bitmap);
        confirmarAltera().show();

    }

    //alert Dialog de confirmação
    private AlertDialog confirmarAltera() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.titulo_confirma))
                .setMessage(getString(R.string.msg_confirma))
                .setPositiveButton(getString(R.string.sim), onSaveMotoboy())
                .setNegativeButton(getString(R.string.nao), onSaveMotoboy());
        return builder.create();
    }

    private DialogInterface.OnClickListener onSaveMotoboy() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //sim
                        Toast.makeText(getContext(), "SIM", Toast.LENGTH_SHORT).show();
                        JsonParser<Motoboy> parserCliente;
                        String motoboyJson;

                        if (motoboy == null) {
                            motoboy = new Motoboy();
                        }

                        if (motoboy.getIdMotoboy() != null) {
                            // update
                            // converte o objeto cliente em json
                            parserCliente = new JsonParser<>(Motoboy.class);
                            motoboyJson = parserCliente.fromObject(motoboy);
                            // executa a task de update (PUT)
                            new TaskRest(TaskRest.RequestMethod.PUT, handlerSalvar).execute(String.format(RestAddress.atualizaMotoboy, motoboy.getIdMotoboy()), motoboyJson);
                        } else {
                            Toast.makeText(getContext(), "Não foi possivel adicionar sua foto", Toast.LENGTH_SHORT).show();
                            Bitmap bm = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.perfilicone);
                            imgMotoboy.setImageBitmap(bm);
                        }
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //não
                        dialogInterface.dismiss();
                        break;
                }
            }
        };
    }

    private HandlerTask handlerSalvar = new HandlerTaskAdapter() {
        @Override
        public void onSuccess(String readValue) {
            super.onSuccess(readValue);
            Toast.makeText(getContext(), "Foto Salva com sucesso", Toast.LENGTH_SHORT).show();
            clear();
        }

        @Override
        public void onError(Exception erro) {
            super.onError(erro);
            Toast.makeText(getContext(), erro.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    private void clear() {
        Bitmap bm = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.perfilicone);
        imgMotoboy.setImageBitmap(bm);

    }*/


}
