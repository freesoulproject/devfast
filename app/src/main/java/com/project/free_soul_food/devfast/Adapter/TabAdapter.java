package com.project.free_soul_food.devfast.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.project.free_soul_food.devfast.Fragments.EntregasFragment;
import com.project.free_soul_food.devfast.Fragments.MapaFragment;
import com.project.free_soul_food.devfast.Fragments.MotoboyFragment;

/**
 * Created by Free_Soul_Food on 13/09/2017.
 */

public class TabAdapter extends FragmentPagerAdapter {
    int numOfTabs;

    public TabAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                //instancia o fragment de Mapa
                MapaFragment mapaFragment = new MapaFragment();

                return mapaFragment;
            case 1:
                EntregasFragment entregasFragment = new EntregasFragment();
                return entregasFragment;
            case 2:
                MotoboyFragment motoboyFragment = new MotoboyFragment();
                return motoboyFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs != 0 ? numOfTabs : 0;
    }
}
