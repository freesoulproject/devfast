package com.project.free_soul_food.devfast.Model;

import com.project.free_soul_food.devfast.Enum.NivelFragilidade;
import com.project.free_soul_food.devfast.Enum.Tamanho;

/**
 * Created by Free_Soul_Food on 15/09/2017.
 */

public class Entrega {
    private Long idEntrega;
    private String descricao;
    private Tamanho tamanho;
    private NivelFragilidade fragilidade;
    private String telefoneCliente;
    private Double valorTotal;
    private Endereco endereco;
    private Restaurante restaurante;
    private String nomeCliente;

    public Long getIdEntrega() {
        return idEntrega;
    }

    public void setIdEntrega(Long idEntrega) {
        this.idEntrega = idEntrega;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Tamanho getTamanho() {
        return tamanho;
    }

    public void setTamanho(Tamanho tamanho) {
        this.tamanho = tamanho;
    }

    public NivelFragilidade getFragilidade() {
        return fragilidade;
    }

    public void setFragilidade(NivelFragilidade fragilidade) {
        this.fragilidade = fragilidade;
    }

    public String getTelefoneCliente() {
        return telefoneCliente;
    }

    public void setTelefoneCliente(String telefoneCliente) {
        this.telefoneCliente = telefoneCliente;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Restaurante getRestaurante() {
        return restaurante;
    }

    public void setRestaurante(Restaurante restaurante) {
        this.restaurante = restaurante;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }
}
