package com.project.free_soul_food.devfast.Rest;

/**
 * Created by Free_Soul_Food on 18/09/2017.
 */

public class RestAddress {
    public static String protocolo = "http://";
    public static String servidor = "172.16.12.18";
    public static String entrega = "/entrega";
    public static String site = "/FastDelivery";
    public static String urlService = protocolo + servidor + site;

    // Recursos da API
    public static String listaMotoboy = urlService + "/motoboy";
    public static String salvaMotoboy = urlService + "/motoboy/";
    public static String atualizaMoto = urlService + "/motoboyRest/atualizaMoto";
    public static String atualizaMotoboy = urlService + "/motoboy/atualizaMotoboy";
    public static String pesquisaMotoboy = urlService + "/motoboy/%d";
    public static String logar = urlService + "/login/logarMotoboy";

    public static String listaEntrega = urlService + entrega + "/listarEntrega";
    public static String salvaEntrega = urlService + "/entrega/";
    public static String atualizaEntrega = urlService + "/entrega/%d";
    public static String excluiEntrega = urlService + "/entrega/%d";
    public static String pesquisaEntrega = urlService + "/entrega/%d";
}
